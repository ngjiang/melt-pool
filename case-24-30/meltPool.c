/**
#Modified from Udaykumar.c
#Solidification minimum working example

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_2 \nabla T_S)
$$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

This test case was described in [Udaykumar et al.](#Udaykumar1999), initial
temperature is :

$$
\phi(x,y,t_0) = 1- \dfrac{erf(\dfrac{1-y}{2\sqrt{t_0}})}{erf(\lambda)}
$$
with $\lambda = 0.9$, $t_0 = 0.25$.

The initial position of the interface is  $1- 2\lambda \sqrt{t_0}$


The interface position should be :
$$
S(t) = 1- 2\lambda \sqrt{t_0}
$$

What is interesting about this case is that we test the ability of
embed_extrapolate to initialize the temperature in newly liquid cells.

~~~gnuplot Position of the interface
set term svg size 1000,1000
set key right
plot 'log0' u 1:2 w p pt 7 ps 0.5 lc 'blue'  t '32x32',\
     'log1' u 1:2 w p pt 7 ps 0.5 lc 'green' t '64x64',\
     'log2' u 1:2 w p pt 7 ps 0.5 lc 'red'   t '128x128',\
     'log3' u 1:2 w p pt 7 ps 0.5 lc 'red'   t '256x256',\
     '' u 1:3 w l dt 2 lw 1.5 t 'reference'
~~~

~~~gnuplot Average error convergence CFL = 0.5
set term svg size 600,600
unset xrange
unset yrange

ftitle(a,b) = sprintf("%.3f/x^{%4.2f}", exp(a), -b)

f(x) = a + b*x
fit f(x) 'log' u (log($1)):(log($2)) via a,b

f2(x) = a2 + b2*x
fit f2(x) 'log' u (log($1)):(log($4)) via a2,b2

f3(x) = a3 + b3*x
fit f3(x) 'log' u (log($1)):(log($6)) via a3,b3

set ylabel 'Average error'
set xlabel 'number of cells per direction, grid NxN'
set xrange [16:512]
set yrange [*:*]
set xtics 16,2,512
set format y "%.1e"
set logscale
  plot '' u 1:2 pt 7 lc 'blue' t 'all cells', exp(f(log(x))) t ftitle(a,b) lc 'blue', \
       '' u 1:4 pt 7 lc 'green' t 'partial cells', exp(f2(log(x))) t ftitle(a2,b2) lc 'green', \
       '' u 1:6 pt 7 lc 'red' t 'full cells', exp(f3(log(x))) t ftitle(a3,b3) lc 'red'
~~~

As convergence analysis shows, we get a second order accuracy.


~~~gnuplot Initial and final temperature plots
set term svg size 1000,1000
set key left
unset logscale
unset xrange
unset xtics
set grid
set ylabel 'Temperature profile' 
set yrange[-0.1:1]
set xrange [0.169:1]
set xtics 0.15,0.1,1
plot 'out0' u 1:2 w p pt 7 lc 'blue' t 'Final temperature 32x32  ',\
     'out1' u 1:2 w p pt 7 lc 'green' t '64x64  ',\
     'out2' u 1:2 w p pt 7 lc 'red' t '128x128', \
     'out3' u 1:2 w p pt 7 lc 'red' t '256x256', \
     '' u 1:3 w l lt -1 t 'Reference final',\
     'init0' u 1:2 w p pt 7 lc 'blue' t 'Initial temperature 32x32 ',\
     'init1' u 1:2 w p pt 7 lc 'green' t '64x64  ',\
     'init2' u 1:2 w p pt 7 lc 'red' t '128x128  ',\
     'init3' u 1:2 w p pt 7 lc 'red' t '256x256  ',\
     '' u 1:3 w l lt -1 t 'Reference initial'
~~~

~~~gnuplot Final error plots
set term svg size 600,600
set key left
set logscale y
set ylabel 'Error profile'
unset yrange
plot 'out0' u 1:4 w p pt 7 lc 'blue' t '32x32',\
     'out1' u 1:4 w p pt 7 lc 'green' t '64x64',\
     'out2' u 1:4 w p pt 7 lc 'red' t '128x128',\
     'out3' u 1:4 w p pt 7 lc 'red' t '256x256'
~~~
*/
#define Gibbs_Thomson 0
#define QUADRATIC 1
#define GHIGO 1

#if 0
#if GHIGO
#include "ghigo/src/myembed.h"
#else
#include "embed.h"
#endif
#include "alimare/double_embed-tree.h"

#include "alimare/advection_A.h"
#if GHIGO
#include "ghigo/src/mydiffusion.h"
#else
#include "diffusion.h"
#endif
#endif

#if 1
#include "ghigo/src/myembed.h"
#include "alimare/embed_extrapolate_3.h"
#include "alimare/double_embed-tree.h"
#include "navier-stokes/centered.h"
#include "tracer.h"
#include "diffusion.h"
#endif

#include "fractions.h"
#include "curvature.h"

#include "alimare/level_set.h"
#include "alimare/LS_advection.h"
#include "view.h"

//added line
//#include "navier-stokes/perfs.h"

#define T_eq          0.
#define TL_inf        1.
#define TS_inf        -1.
//#define X_laser       0.5    // position of laser beam center
#define W_laser       0.0338000
//3.38e-3    // width of laser zone, should be smaller than 1. 
#define DT_laser      97.977     // temperature gradient caused by laser.
#define V_LASER       57.049

int MINLEVEL, MAXLEVEL; 
double latent_heat;
char filename [100];
FILE * fp1, * fp2, * fp3;

#define DT_MAX  1.

#define T_eq         0.

// Definition of plane() by macro: upper side positive
#define PLANE(Y, H0) (Y -H0)
// Definition of circle by macro: inner side positive
#define CIRCLE(X0, Y0, R) (R*R-(x-X0)*(x-X0)-(y-Y0)*(y-Y0))


#define RAYLEIGH      56356.1 //Rayleigh number Ra
#define PRANDTL       0.07131
//#define Stefan 2.85 //761869 // lbd * exp(lbd**2) * erf(lbd) * sqrt(Pi)
#define STEFAN 1.9251
#define rootlambda 0.9
#define t0 0.001

#define tstart 0.
#define T_END 0.01052
#define T_START 5e-7

#define LASER_CENTRE(TIME) (0.2 + clamp(TIME-T_START,0.,(0.6/V_LASER))*V_LASER)
//#define LASER_CENTRE(TIME) clamp(0.2 + (TIME-T_START)*clamp((TIME-T_START)*1.e6,0.,V_LASER), 0.2, 0.8)

#define SQRT2PI 2.5066282746310002

double T_init(double y,double t,double y0){
  return 1.-erf((1.-y)/(2.*sqrt(t)))/erf(rootlambda);
}

scalar TL[], TS[], dist[];
vector vpc[];
vector vpcf[];

scalar * tracers   = {TL};
scalar * tracers2  = {TS};
scalar * level_set = {dist};
face vector muv[], av[];
mgstats mgT;
scalar grad1[], grad2[];
double DT2;
double S0; // initial position of the interface

scalar curve[];

double lambda[2];

int     nb_cell_NB =  1 << 2 ;  // number of cells for the NB
double  NB_width ;              // length of the NB

double  epsK = 0., epsV = 0.;

double eps4 = 0.;

double s_clean = 1.e-10; // used for fraction cleaning

  
mgstats mgT;

TL[embed] = dirichlet(T_eq);
//TL[top]   = dirichlet(TL_inf); 
//TL[top] = fabs(x - LASER_CENTRE(t)) < W_laser * 0.5 ? neumann(DT_laser) : neumann(0);
TL[top] = neumann(2*DT_laser*exp(-(x - LASER_CENTRE(t))*(x - LASER_CENTRE(t))/(2*0.25*0.25*W_laser*W_laser)));
//TL[top] = neumann(2*DT_laser/(W_laser*0.25*SQRT2PI) * exp(-(x - LASER_CENTRE(t))*(x - LASER_CENTRE(t))/(2*0.25*0.25*W_laser*W_laser)));
//	neumann(DT_laser) : dirichlet(TL_inf);
//TL[left] = neumann(0);
//TL[right] = neumann(0);

TS[embed]  = dirichlet(T_eq);
TS[bottom] = dirichlet(TS_inf); 
//TS[left] = neumann(0);
//TS[right] = neumann(0);
TS[left]   = dirichlet(TS_inf);
TS[right]  = dirichlet(TS_inf);
//TS[top] = fabs(x - LASER_CENTRE(t)) < W_laser * 0.5 ? neumann(DT_laser) : neumann(0);
TS[top] = neumann(2*DT_laser*exp(-(x - LASER_CENTRE(t))*(x - LASER_CENTRE(t))/(2*0.25*0.25*W_laser*W_laser)));

//nu = 0.1;

int j;
int k_loop = 0; // used when a becomes an interfacial cell, we iterate a bit
// more one the Poisson solver.

#include "alimare/alex_functions2.h"



int main() {
  L0 = 1.;
  //periodic(left);
  TL.third = true;
  TS.third = true;

  for (j=5;j<=5;j++){

/**
Here we set up the parameters of our simulation. The latent heat $L_H$, the
initial position of the interface $h_0$ and the resolution of the grid.
*/
    latent_heat  = 1/STEFAN;
    MAXLEVEL =  5+j ;
    //MINLEVEL = MAXLEVEL -1;
    //MAXLEVEL = 8;
    MINLEVEL = 5;
    DT2 = 1.e-2/powf(4,j);
    N = 1 << MAXLEVEL;
    snprintf(filename, 100,  "log%d", j);
    fp1 = fopen (filename,"w");
    fprintf(fp1, "#Size of melting pool\n#Time\tMaximal_Depth\tCentral_Depth\tLeft_Edge\tRight_Edge\tWidth\n");
    snprintf(filename, 100,  "out%d", j);
    fp2 = fopen (filename,"w");
    snprintf(filename, 100,  "init%d", j);
    fp3 = fopen (filename,"w");
    init_grid (N);
    mu = muv;
    a = av;
    run();
    fclose(fp1); 
    fclose(fp2); 
    fclose(fp3);
  }
  exit(1);
}

event init(t=0){

  NB_width = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.1475;

  //S0 = 1.-2*rootlambda*sqrt(t0); // initial position of the interface
  //S0 = 0.95; // initial position of the interface
  foreach(){
    // Define initial Level-set distance function
    //dist[] = clamp(PLANE(y,S0),-1.1*NB_width,1.1*NB_width);
    dist[] = clamp(CIRCLE(LASER_CENTRE(0.),1.,W_laser),-1.1*NB_width,1.1*NB_width);
  }
  boundary ({dist});
  restriction({dist});

  vertex scalar dist_n[];
  cell2node(dist,dist_n);

  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs,s_clean);

  boundary({cs,fs});
  restriction({cs,fs});

  curvature(cs,curve);
  boundary({curve});

  //initial conditions
  foreach() {
    if(cs[]>0.){
      //TL[] = T_eq + clamp(CIRCLE(LASER_CENTRE(0.),1.,W_laser/2.)*1.,0.,1.);
      TL[] = T_eq;
      TS[] = T_eq;
      }
    else {
      TL[] = T_eq;
      //TS[] = T_eq + clamp(CIRCLE(LASER_CENTRE(0.),1.,W_laser/2.)*100.,-1.,0.);
      TS[] = TS_inf;
      }
  }

  #if 0
  foreach(){
    if(x==(0.5+Delta/2.) && cs[] >0){
      fprintf(fp3, "%.8g %.8g %.8g %.8g\n", y, TL[], T_init(y,t0,S0),
       fabs(TL[]-T_init(y,t0,S0)));}
  }
  #endif
  foreach_face(){
    vpc.x[] = 0.;
  }


  boundary({TL,TS});
  restriction({TL,TS});
  myprop(muv,fs,lambda[0]);

#if GHIGO // mandatory for GHIGO
  u.n[embed] = dirichlet(0);
  u.t[embed] = dirichlet(0);

  // Added to account for moving boundary algorithm
  
  uf.n[embed] = dirichlet (0.);
  uf.t[embed] = dirichlet (0.);
#endif
}


event velocity(i++){
  double lambda1 = lambda[0], lambda2 = lambda[1]; 
  LS_speed(
    dist,latent_heat,cs,fs,TS,TL,T_eq,
    vpc,vpcf,lambda1,lambda2,
    epsK,epsV,eps4,deltat=0.45*L0/(1<<MAXLEVEL),
    itrecons = 30,tolrecons = 1.e-5,NB_width);
  DT = timestep_LS(vpcf,DT2,dist,NB_width);
  tnext = t+DT;
  dt = DT;
}
/**
*/


event tracer_diffusion (i++) {
  advection_LS(
  dist,
  cs,fs,
  TS,TL,
  vpcf,
  itredist = 5,
  s_clean = 1.e-10,
  NB_width,
  curve);

  boundary({TL});
  myprop(muv,fs,lambda[0]);
  mgT = diffusion (TL,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[1]);
  boundary({TS});
  mgT = diffusion (TS,dt,muv,theta = cm);
  writefile(mgT);
  invertcs(cs,fs);
  myprop(muv,fs,lambda[0]);
}

// event LS_advection(i++,last){
//   view(tx=  -0.5, ty = -0.5);
//   draw_vof("cs","fs");
//   squares("dist",min = -NB_width ,max = NB_width);
//   squares("dist",min = -NB_width ,max = NB_width);
//   save("dist.mp4");
// }


/**
We output the interface position as a function of time.
*/

 event outputs ( i++,last;t<T_END)
 {
    boundary({TL,TS});
    scalar visu[];
    foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    view (tx = -0.5, ty = -0.5);
    draw_vof("cs");
    squares("visu", min = -1, max = 1);
    save ("visu.mp4");
 
   #if 0
   double y_max=0;
   vector h[];
   heights (cs, h);
   boundary((scalar *){h});
   foreach(reduction(max:y_max)){
     if(interfacial(point, cs) && y >0){
       double yy = y+Delta*height(h.y[]);
       if(yy < 1.e10)y_max = max(y_max,yy);
     }     
   }
   S0 = 1.-2*rootlambda*sqrt(t0+t);
   fprintf(fp1, "%g %g %g\n", t+t0+dt, y_max, S0);
   #endif
   #if 1
   double d_max=0., x1=0., x2=0., d_laser=0.;
   vector h[];
   heights (cs, h);
   boundary((scalar *){h});
   foreach(){
     if(interfacial(point, cs)){
       double xx = x + Delta*height(h.x[]);
       if(y == 1.-Delta/2.){
         //if(x1<1.e-5) x1 = x;
         //else if(x<x1) x2 = x1, x1 = x;
         //else x2 = x;
         //double xx = x + Delta*height(h.x[]);
         if(xx < LASER_CENTRE(t)) x1 = xx;
         if(xx > LASER_CENTRE(t)) x2 = xx;
       }
       double yy = y + Delta*height(h.y[]);
       if(1.- yy > d_max) d_max = 1. - yy;
       if(fabs(x-LASER_CENTRE(t)) < Delta/2.) d_laser = 1.-yy;
     }
   }
   fprintf(fp1, "%g\t%g\t%g\t%g\t%g\t%g\n", t, d_max, d_laser, x1, x2, x2-x1);
   #endif
 }


event final_error(t=T_END){
  scalar e[], ep[], ef[];
  S0 = 1.-2*rootlambda*sqrt(t0+t);
  foreach() {
    if (cs[] <= 0.)
      ef[] = ep[] = e[] = nodata;
    else {
      e[]  = TL[] - T_init(y,t0+t,S0);
      ep[] = cs[] < 1. ? e[] : nodata;
      ef[] = cs[] >= 1. ? e[] : nodata;
    }
  }
  // Modify these lines to show pool width
  foreach(){
    if(x==0.5+Delta/2. && cs[] > 0){
      fprintf(fp2, "%.8g %.8g %.8g %.8g\n", y, TL[], T_init(y,t0+t,S0),
        fabs(TL[]-T_init(y,t0+t,S0)));
    }
  }
  //
  norm n = normf (e), np = normf (ep), nf = normf (ef);
  fprintf (stderr, "%d %3.3e %3.3e %3.3e %3.3e %3.3e %3.3e %e\n",
   N, n.avg, n.max, np.avg, np.max, nf.avg, nf.max, dt);
  fflush (ferr);
}


event properties (i++){
  foreach_face()
    muv.x[] = fm.x[]*PRANDTL;
  boundary((scalar*){muv});
}

/**
Boussinesq term.
*/
event acceleration (i++) {
  foreach_face(x)
    av.x[] = 0;
  foreach_face(y)
    av.y[] = cs[]*RAYLEIGH*PRANDTL*(TL[]+TL[-1])/2.;
  boundary((scalar *) {av});

}

#if 0
// test
event test(i++)
  fprintf(stderr,"test\n");

// Debug output
// bview2D file65
event debug(i++;i<10){
  snprintf(filename, 100,  "debug%d", i);
  dump(filename);
}
#endif

#if 1
event adapt (i++, last) {
  // if(i%10==0)fprintf(stderr, "##%g %d\n", t, grid->n);
  foreach_cell(){
    cs2[] = 1.-cs[];
  }
  foreach_face(){
    fs2.x[]      = 1.-fs.x[];
  }

  boundary({cs,cs2,fs,fs2});
  fractions_cleanup(cs,fs,smin = s_clean);
  fractions_cleanup(cs2,fs2,smin = s_clean);
  restriction({cs,cs2,fs,fs2});
  boundary({TL,TS});
  scalar visu[], atanT[];
  foreach(){
    visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    atanT[] = atan(visu[]*50.);
  }
  boundary({visu,atanT});
  restriction({visu,atanT});

  adapt_wavelet ({cs,visu,atanT,vpcf.x, vpcf.y},
    (double[]){1.e-3,3.e-2,5.e-3,5.e-3,5.e-3},MAXLEVEL, MINLEVEL);
}
#endif

#if 0
event movie(i+=10,last){
  if(MAXLEVEL==6){
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});
    squares("visu");
    save("visu.mp4");
  }
}
#endif


/**
~~~bib
@Article{Udaykumar1999,
  author        = {H. S. Udaykumar and R. Mittal and Wei Shyy},
  title         = {Computation of Solid–Liquid Phase Fronts in the Sharp Interface Limit on Fixed Grids},
  year          = {1999},
  volume        = {153},
  pages         = {535-574},
  issn          = {0021-9991},
  doi           = {10.1006/jcph.1999.6294},
}

~~~
*/
