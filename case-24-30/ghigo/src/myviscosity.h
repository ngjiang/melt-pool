<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Basilisk - sandbox/ghigo/src/myviscosity.h</title>
        <link href="/css/custom.css" rel="stylesheet" media="screen, projection" type="text/css" />
    <link href="/css/print.css" rel="stylesheet" media="print" type= "text/css" />
        <!--[if IE]><link href="/css/ie.css" rel="stylesheet" media="screen, projection" type="text/css" /><![endif]-->
    <link rel="stylesheet" href="/css/basilisk.css"/>
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui.packed.js" type="text/javascript"></script>
    <script src="/js/plots.js" type="text/javascript"></script>
<link rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/katex.min.css"
      integrity="sha384-yFRtMMDnQtDRO8rLpMIKrtPCD5jdktao2TV19YiZYWMDkUR5GQZR/NOVTdquEx1j"
      crossorigin="anonymous"/>
<script defer
	src="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/katex.min.js"
	integrity="sha384-9Nhn55MVVN0/4OFx7EE5kpFBPsEMZxKTCnA+4fqDmg12eCTqGi6+BB2LjY8brQxJ"
	crossorigin="anonymous">
</script>
  </head>
  <body>
    <div id="doc3" class="yui-t1">
        <div id="yui-main">
          <div id="maincol" class="yui-b">
<div id="userbox">
  <noscript>
    <a href="/_login">Login</a>
    <a href="/_logout">Logout</a>
  </noscript>
  &nbsp;
  <a id="loginlink" class="login" href="/_login">Login / Get an account</a>
  <a id="logoutlink" class="login" href="/_logout">Logout <span id="logged_in_user"></span></a>
</div>
<ul class="tabs">
  <li class=selected><a href="/sandbox/ghigo/src/myviscosity.h">view</a></li><li><a href="/_edit/sandbox/ghigo/src/myviscosity.h">edit</a></li><li><a href="/sandbox/ghigo/src/myviscosity.h?history">history</a></li>
</ul>
<div id="content">
        <h1 class="pageTitle"><a href="/sandbox/ghigo/src/myviscosity.h">sandbox/ghigo/src/myviscosity.h</a></h1>
  <ul class="messages" id="messages"></ul>
  <div id="status"></div>
    <div class="sourceCode"><table class="sourceCode c numberLines lineAnchors"><tr class="sourceCode"><td class="lineNumbers"><pre><a id="1" href="#1">1</a>
<a id="2" href="#2">2</a>
<a id="3" href="#3">3</a>
<a id="4" href="#4">4</a>
<a id="5" href="#5">5</a>
<a id="6" href="#6">6</a>
<a id="7" href="#7">7</a>
<a id="8" href="#8">8</a>
<a id="9" href="#9">9</a>
<a id="10" href="#10">10</a>
<a id="11" href="#11">11</a>
<a id="12" href="#12">12</a>
<a id="13" href="#13">13</a>
<a id="14" href="#14">14</a>
<a id="15" href="#15">15</a>
<a id="16" href="#16">16</a>
<a id="17" href="#17">17</a>
<a id="18" href="#18">18</a>
<a id="19" href="#19">19</a>
<a id="20" href="#20">20</a>
<a id="21" href="#21">21</a>
<a id="22" href="#22">22</a>
<a id="23" href="#23">23</a>
<a id="24" href="#24">24</a>
<a id="25" href="#25">25</a>
<a id="26" href="#26">26</a>
<a id="27" href="#27">27</a>
<a id="28" href="#28">28</a>
<a id="29" href="#29">29</a>
<a id="30" href="#30">30</a>
<a id="31" href="#31">31</a>
<a id="32" href="#32">32</a>
<a id="33" href="#33">33</a>
<a id="34" href="#34">34</a>
<a id="35" href="#35">35</a>
<a id="36" href="#36">36</a>
<a id="37" href="#37">37</a>
<a id="38" href="#38">38</a>
<a id="39" href="#39">39</a>
<a id="40" href="#40">40</a>
<a id="41" href="#41">41</a>
<a id="42" href="#42">42</a>
<a id="43" href="#43">43</a>
<a id="44" href="#44">44</a>
<a id="45" href="#45">45</a>
<a id="46" href="#46">46</a>
<a id="47" href="#47">47</a>
<a id="48" href="#48">48</a>
<a id="49" href="#49">49</a>
<a id="50" href="#50">50</a>
<a id="51" href="#51">51</a><span id="relax_viscosity"/>
<a id="52" href="#52">52</a>
<a id="53" href="#53">53</a>
<a id="54" href="#54">54</a>
<a id="55" href="#55">55</a>
<a id="56" href="#56">56</a>
<a id="57" href="#57">57</a>
<a id="58" href="#58">58</a>
<a id="59" href="#59">59</a>
<a id="60" href="#60">60</a>
<a id="61" href="#61">61</a>
<a id="62" href="#62">62</a>
<a id="63" href="#63">63</a>
<a id="64" href="#64">64</a>
<a id="65" href="#65">65</a>
<a id="66" href="#66">66</a>
<a id="67" href="#67">67</a>
<a id="68" href="#68">68</a>
<a id="69" href="#69">69</a>
<a id="70" href="#70">70</a>
<a id="71" href="#71">71</a>
<a id="72" href="#72">72</a>
<a id="73" href="#73">73</a>
<a id="74" href="#74">74</a>
<a id="75" href="#75">75</a>
<a id="76" href="#76">76</a>
<a id="77" href="#77">77</a>
<a id="78" href="#78">78</a>
<a id="79" href="#79">79</a>
<a id="80" href="#80">80</a>
<a id="81" href="#81">81</a>
<a id="82" href="#82">82</a>
<a id="83" href="#83">83</a>
<a id="84" href="#84">84</a>
<a id="85" href="#85">85</a>
<a id="86" href="#86">86</a>
<a id="87" href="#87">87</a>
<a id="88" href="#88">88</a>
<a id="89" href="#89">89</a>
<a id="90" href="#90">90</a>
<a id="91" href="#91">91</a>
<a id="92" href="#92">92</a>
<a id="93" href="#93">93</a>
<a id="94" href="#94">94</a>
<a id="95" href="#95">95</a>
<a id="96" href="#96">96</a>
<a id="97" href="#97">97</a>
<a id="98" href="#98">98</a>
<a id="99" href="#99">99</a>
<a id="100" href="#100">100</a>
<a id="101" href="#101">101</a>
<a id="102" href="#102">102</a>
<a id="103" href="#103">103</a>
<a id="104" href="#104">104</a>
<a id="105" href="#105">105</a>
<a id="106" href="#106">106</a>
<a id="107" href="#107">107</a>
<a id="108" href="#108">108</a>
<a id="109" href="#109">109</a>
<a id="110" href="#110">110</a>
<a id="111" href="#111">111</a>
<a id="112" href="#112">112</a>
<a id="113" href="#113">113</a>
<a id="114" href="#114">114</a>
<a id="115" href="#115">115</a>
<a id="116" href="#116">116</a>
<a id="117" href="#117">117</a>
<a id="118" href="#118">118</a>
<a id="119" href="#119">119</a>
<a id="120" href="#120">120</a><span id="residual_viscosity"/>
<a id="121" href="#121">121</a>
<a id="122" href="#122">122</a>
<a id="123" href="#123">123</a>
<a id="124" href="#124">124</a>
<a id="125" href="#125">125</a>
<a id="126" href="#126">126</a>
<a id="127" href="#127">127</a>
<a id="128" href="#128">128</a>
<a id="129" href="#129">129</a>
<a id="130" href="#130">130</a>
<a id="131" href="#131">131</a>
<a id="132" href="#132">132</a>
<a id="133" href="#133">133</a>
<a id="134" href="#134">134</a>
<a id="135" href="#135">135</a>
<a id="136" href="#136">136</a>
<a id="137" href="#137">137</a>
<a id="138" href="#138">138</a>
<a id="139" href="#139">139</a>
<a id="140" href="#140">140</a>
<a id="141" href="#141">141</a>
<a id="142" href="#142">142</a>
<a id="143" href="#143">143</a>
<a id="144" href="#144">144</a>
<a id="145" href="#145">145</a>
<a id="146" href="#146">146</a>
<a id="147" href="#147">147</a>
<a id="148" href="#148">148</a>
<a id="149" href="#149">149</a>
<a id="150" href="#150">150</a>
<a id="151" href="#151">151</a>
<a id="152" href="#152">152</a>
<a id="153" href="#153">153</a>
<a id="154" href="#154">154</a>
<a id="155" href="#155">155</a>
<a id="156" href="#156">156</a>
<a id="157" href="#157">157</a>
<a id="158" href="#158">158</a>
<a id="159" href="#159">159</a>
<a id="160" href="#160">160</a>
<a id="161" href="#161">161</a>
<a id="162" href="#162">162</a>
<a id="163" href="#163">163</a>
<a id="164" href="#164">164</a>
<a id="165" href="#165">165</a>
<a id="166" href="#166">166</a>
<a id="167" href="#167">167</a>
<a id="168" href="#168">168</a>
<a id="169" href="#169">169</a>
<a id="170" href="#170">170</a>
<a id="171" href="#171">171</a>
<a id="172" href="#172">172</a>
<a id="173" href="#173">173</a>
<a id="174" href="#174">174</a>
<a id="175" href="#175">175</a>
<a id="176" href="#176">176</a>
<a id="177" href="#177">177</a>
<a id="178" href="#178">178</a>
<a id="179" href="#179">179</a>
<a id="180" href="#180">180</a>
<a id="181" href="#181">181</a>
<a id="182" href="#182">182</a>
<a id="183" href="#183">183</a>
<a id="184" href="#184">184</a>
<a id="185" href="#185">185</a>
<a id="186" href="#186">186</a>
<a id="187" href="#187">187</a>
<a id="188" href="#188">188</a>
<a id="189" href="#189">189</a>
<a id="190" href="#190">190</a>
<a id="191" href="#191">191</a>
<a id="192" href="#192">192</a>
<a id="193" href="#193">193</a>
<a id="194" href="#194">194</a>
<a id="195" href="#195">195</a>
<a id="196" href="#196">196</a>
<a id="197" href="#197">197</a>
<a id="198" href="#198">198</a>
<a id="199" href="#199">199</a>
<a id="200" href="#200">200</a>
<a id="201" href="#201">201</a>
<a id="202" href="#202">202</a>
<a id="203" href="#203">203</a>
<a id="204" href="#204">204</a><span id="viscosity"/>
<a id="205" href="#205">205</a>
<a id="206" href="#206">206</a>
<a id="207" href="#207">207</a>
<a id="208" href="#208">208</a>
<a id="209" href="#209">209</a>
<a id="210" href="#210">210</a>
<a id="211" href="#211">211</a>
<a id="212" href="#212">212</a>
<a id="213" href="#213">213</a>
<a id="214" href="#214">214</a>
<a id="215" href="#215">215</a>
<a id="216" href="#216">216</a>
<a id="217" href="#217">217</a>
<a id="218" href="#218">218</a>
<a id="219" href="#219">219</a>
<a id="220" href="#220">220</a>
<a id="221" href="#221">221</a>
<a id="222" href="#222">222</a>
<a id="223" href="#223">223</a>
</pre></td><td class="sourceCode"><pre><code class="sourceCode c"><span class="pp">#include </span><span class="im">&quot;mypoisson.h&quot;</span>
<a id="" href="#"></a>
<span class="kw">struct</span> Viscosity {
  vector u;
  face vector mu;
  scalar rho;
  <span class="dt">double</span> dt;
  <span class="dt">int</span> nrelax;
  scalar * res;
<span class="pp">#if EMBED</span>
  <span class="dt">void</span> (* embed_stress_flux) (Point, vector, vector, coord *, coord *);
<span class="pp">#endif </span><span class="co">// EMBED</span>
};
<a id="" href="#"></a>
<span class="pp">#if AXI</span>
<span class="co">// fixme: RHO here not correct</span>
<span class="pp"># define lambda ((coord){1., 1. + dt/RHO*(mu.x[] + mu.x[1] + \</span>
<span class="pp">					  mu.y[] + mu.y[0,1])/2./sq(y)})</span>
<span class="pp">#else </span><span class="co">// not AXI</span>
<span class="pp"># if dimension == 1</span>
<span class="pp">#   define lambda ((coord){1.})</span>
<span class="pp"># elif dimension == 2</span>
<span class="pp">#   define lambda ((coord){1.,1.})</span>
<span class="pp"># elif dimension == 3</span>
<span class="pp">#   define lambda ((coord){1.,1.,1.})</span>
<span class="pp">#endif</span>
<span class="pp">#endif</span>
<a id="" href="#"></a>
<span class="co">// Temporary placement for tangential face gradients</span>
<a id="" href="#"></a>
<span class="pp">#ifndef EMBED</span>
<span class="pp">#define face_avg_gradient_t1_x(a,i)				\</span>
<span class="pp">  ((a[1,i-1] + a[1,i] - a[-1,i-1] - a[-1,i])/(4.*Delta))</span>
<span class="pp">#define face_avg_gradient_t2_x(a,i) \</span>
<span class="pp">  ((a[1,0,i-1] + a[1,0,i] - a[-1,0,i-1] - a[-1,0,i])/(4.*Delta))</span>
<a id="" href="#"></a>
<span class="pp">#define face_avg_gradient_t1_y(a,i) \</span>
<span class="pp">  ((a[i-1,1] + a[i,1] - a[i-1,-1] - a[i,-1])/(4.*Delta))</span>
<span class="pp">#define face_avg_gradient_t2_y(a,i) \</span>
<span class="pp">  ((a[0,1,i-1] + a[0,1,i] - a[0,-1,i-1] - a[0,-1,i])/(4.*Delta))</span>
<a id="" href="#"></a>
<span class="pp">#define face_avg_gradient_t1_z(a,i) \</span>
<span class="pp">  ((a[i-1,0,1] + a[i,0,1] - a[i-1,0,-1] - a[i,0,-1])/(4.*Delta))</span>
<span class="pp">#define face_avg_gradient_t2_z(a,i) \</span>
<span class="pp">  ((a[0,i-1,1] + a[0,i,1] - a[i-1,0,-1] - a[0,i,-1])/(4.*Delta))</span>
<span class="pp">#endif </span><span class="co">// EMBED</span>
<a id="" href="#"></a>
<span class="co">// Note how the relaxation function uses &quot;naive&quot; gradients i.e. not</span>
<span class="co">// the face_gradient_* macros.</span>
<a id="" href="#"></a>
<span class="dt">static</span> <span class="dt">void</span> relax_viscosity (scalar * a, scalar * b, <span class="dt">int</span> l, <span class="dt">void</span> * data)
{
  <span class="kw">struct</span> Viscosity * p = (<span class="kw">struct</span> Viscosity *) data;
  (<span class="dt">const</span>) face vector mu = p-&gt;mu;
  (<span class="dt">const</span>) scalar rho = p-&gt;rho;
  <span class="dt">double</span> dt = p-&gt;dt;
  vector u = vector(a[<span class="dv">0</span>]), r = vector(b[<span class="dv">0</span>]);
<a id="" href="#"></a>
<span class="pp">#if EMBED</span>
  <span class="dt">void</span> (* embed_stress_flux) (Point, vector, vector,
			      coord *, coord *) = p-&gt;embed_stress_flux;
<span class="pp">#endif </span><span class="co">// EMBED</span>
<a id="" href="#"></a>
<span class="pp">#if JACOBI</span>
  vector w[];
<span class="pp">#else</span>
  vector w = u;
<span class="pp">#endif</span>
<a id="" href="#"></a>
  foreach_level_or_leaf (l) {
    coord c = {<span class="dv">0</span>., <span class="dv">0</span>., <span class="dv">0</span>.}, d = {<span class="dv">0</span>., <span class="dv">0</span>., <span class="dv">0</span>.};
<span class="pp">#if EMBED</span>
    <span class="cf">if</span> (embed_stress_flux)
      embed_stress_flux (point, u, mu, &amp;c, &amp;d);
<span class="pp">#endif </span><span class="co">// EMBED</span>
    foreach_dimension() {
      w.x[] = (dt*(<span class="dv">2</span>.*mu.x[<span class="dv">1</span>]*u.x[<span class="dv">1</span>] + <span class="dv">2</span>.*mu.x[]*u.x[-<span class="dv">1</span>]
               <span class="pp">#if dimension &gt; 1</span>
		   + mu.y[<span class="dv">0</span>,<span class="dv">1</span>]*(u.x[<span class="dv">0</span>,<span class="dv">1</span>] +
				face_avg_gradient_t1_x (u.y, <span class="dv">1</span>)*Delta)
		   - mu.y[]*(- u.x[<span class="dv">0</span>,-<span class="dv">1</span>] +
			     face_avg_gradient_t1_x (u.y, <span class="dv">0</span>)*Delta)
               <span class="pp">#endif</span>
	       <span class="pp">#if dimension &gt; 2</span>
		   + mu.z[<span class="dv">0</span>,<span class="dv">0</span>,<span class="dv">1</span>]*(u.x[<span class="dv">0</span>,<span class="dv">0</span>,<span class="dv">1</span>] +
				  face_avg_gradient_t2_x (u.z, <span class="dv">1</span>)*Delta)
		   - mu.z[]*(- u.x[<span class="dv">0</span>,<span class="dv">0</span>,-<span class="dv">1</span>] +
			     face_avg_gradient_t2_x (u.z, <span class="dv">0</span>)*Delta)
               <span class="pp">#endif</span>
		   ) + (r.x[] - dt*c.x)*sq(Delta))/
	(sq(Delta)*(rho[]*lambda.x + dt*d.x) + dt*(<span class="dv">2</span>.*mu.x[<span class="dv">1</span>] + <span class="dv">2</span>.*mu.x[]
                                    <span class="pp">#if dimension &gt; 1</span>
						   + mu.y[<span class="dv">0</span>,<span class="dv">1</span>] + mu.y[]
                                    <span class="pp">#endif</span>
			            <span class="pp">#if dimension &gt; 2</span>
						   + mu.z[<span class="dv">0</span>,<span class="dv">0</span>,<span class="dv">1</span>] + mu.z[]
			            <span class="pp">#endif</span>
						   ) + SEPS);
    }
  }
<a id="" href="#"></a>
<span class="pp">#if JACOBI</span>
  foreach_level_or_leaf (l)
    foreach_dimension()
      u.x[] = (u.x[] + <span class="dv">2</span>.*w.x[])/<span class="dv">3</span>.;
<span class="pp">#endif</span>
<a id="" href="#"></a>
<span class="pp">#if TRASH</span>
  vector u1[];
  foreach_level_or_leaf (l)
    foreach_dimension()
      u1.x[] = u.x[];
  trash ({u});
  foreach_level_or_leaf (l)
    foreach_dimension()
      u.x[] = u1.x[];
<span class="pp">#endif</span>
}
<a id="" href="#"></a>
<span class="dt">static</span> <span class="dt">double</span> residual_viscosity (scalar * a, scalar * b, scalar * resl, 
				  <span class="dt">void</span> * data)
{
  <span class="kw">struct</span> Viscosity * p = (<span class="kw">struct</span> Viscosity *) data;
  (<span class="dt">const</span>) face vector mu = p-&gt;mu;
  (<span class="dt">const</span>) scalar rho = p-&gt;rho;
  <span class="dt">double</span> dt = p-&gt;dt;
  vector u = vector(a[<span class="dv">0</span>]), r = vector(b[<span class="dv">0</span>]), res = vector(resl[<span class="dv">0</span>]);
  <span class="dt">double</span> maxres = <span class="dv">0</span>.;
<a id="" href="#"></a>
<span class="pp">#if EMBED</span>
  <span class="dt">void</span> (* embed_stress_flux) (Point, vector, vector, coord *, coord *) = p-&gt;embed_stress_flux;
<span class="pp">#endif</span>
<a id="" href="#"></a>
<span class="pp">#if TREE</span>
  <span class="co">/* conservative coarse/fine discretisation (2nd order) */</span>
  foreach_dimension() {
    face vector taux[];
    foreach_face(x)
      taux.x[] = <span class="dv">2</span>.*mu.x[]*face_gradient_x (u.x, <span class="dv">0</span>);
    <span class="pp">#if dimension &gt; 1</span>
      foreach_face(y)
	taux.y[] = mu.y[]*(face_gradient_y (u.x, <span class="dv">0</span>) + 
			   face_avg_gradient_t1_x (u.y, <span class="dv">0</span>));
    <span class="pp">#endif</span>
    <span class="pp">#if dimension &gt; 2</span>
      foreach_face(z)
	taux.z[] = mu.z[]*(face_gradient_z (u.x, <span class="dv">0</span>) + 
			   face_avg_gradient_t2_x (u.z, <span class="dv">0</span>));
    <span class="pp">#endif</span>
    boundary_flux ({taux});
    foreach (reduction(max:maxres)) {
      <span class="dt">double</span> a = <span class="dv">0</span>.;
      coord c = {<span class="dv">0</span>., <span class="dv">0</span>., <span class="dv">0</span>.}, d = {<span class="dv">0</span>., <span class="dv">0</span>., <span class="dv">0</span>.};
<span class="pp">#if EMBED</span>
      <span class="cf">if</span> (embed_stress_flux)
	embed_stress_flux (point, u, mu, &amp;c, &amp;d);
<span class="pp">#endif </span><span class="co">// EMBED</span>
      foreach_dimension()
	a += taux.x[<span class="dv">1</span>] - taux.x[];
      res.x[] = r.x[] - rho[]*lambda.x*u.x[] + dt*(a/Delta - (c.x + d.x*u.x[]));
      <span class="cf">if</span> (fabs (res.x[]) &gt; maxres)
	maxres = fabs (res.x[]);
    }
  }
  boundary (resl);
<span class="pp">#else</span>
  <span class="co">/* &quot;naive&quot; discretisation (only 1st order on trees) */</span>
  foreach (reduction(max:maxres)) {
    coord c = {<span class="dv">0</span>., <span class="dv">0</span>., <span class="dv">0</span>.}, d = {<span class="dv">0</span>., <span class="dv">0</span>., <span class="dv">0</span>.};
<span class="pp">#if EMBED</span>
  <span class="cf">if</span> (embed_stress_flux)
    embed_stress_flux (point, u, mu, &amp;c, &amp;d);
<span class="pp">#endif </span><span class="co">// EMBED    </span>
    foreach_dimension() {
      res.x[] = r.x[] - rho[]*lambda.x*u.x[] +
        dt*(<span class="dv">2</span>.*mu.x[<span class="dv">1</span>,<span class="dv">0</span>]*face_gradient_x (u.x, <span class="dv">1</span>)
	    - <span class="dv">2</span>.*mu.x[]*face_gradient_x (u.x, <span class="dv">0</span>)
        <span class="pp">#if dimension &gt; 1</span>
	    + mu.y[<span class="dv">0</span>,<span class="dv">1</span>]* (face_gradient_y (u.x, <span class="dv">1</span>) +
			  face_avg_gradient_t1_x (u.y, <span class="dv">1</span>))
	    - mu.y[]*(face_gradient_y (u.x, <span class="dv">0</span>) +
		      face_avg_gradient_t1_x (u.y, <span class="dv">0</span>))
	<span class="pp">#endif</span>
        <span class="pp">#if dimension &gt; 2</span>
	    + mu.z[<span class="dv">0</span>,<span class="dv">0</span>,<span class="dv">1</span>]*(face_gradient_z (u.x, <span class="dv">1</span>) +
			   face_avg_gradient_t2_x (u.z, <span class="dv">1</span>))
	    - mu.z[]*(face_gradient_z (u.x, <span class="dv">0</span>) +
		      face_avg_gradient_t2_x (u.z, <span class="dv">0</span>))
	<span class="pp">#endif</span>
	    )/Delta - dt*(c.x + d.x*u.x[]);
      <span class="cf">if</span> (fabs (res.x[]) &gt; maxres)
	maxres = fabs (res.x[]);
    }
  }
<span class="pp">#endif</span>
  <span class="cf">return</span> maxres;
}
<a id="" href="#"></a>
<span class="pp">#undef lambda</span>
<a id="" href="#"></a>
<span class="dt">double</span> TOLERANCE_MU = <span class="dv">0</span>.; <span class="co">// default to TOLERANCE</span>
<a id="" href="#"></a>
trace
mgstats viscosity (<span class="kw">struct</span> Viscosity p)
{
  vector u = p.u, r[];
  scalar rho = p.rho;  
  foreach()
    foreach_dimension()
      r.x[] = rho[]*u.x[];
<a id="" href="#"></a>
  face vector mu = p.mu;
  restriction ({mu, rho});
<a id="" href="#"></a>
<span class="pp">#if EMBED</span>
  p.embed_stress_flux = u.x.boundary[embed] != antisymmetry ? embed_stress_flux : NULL;
<span class="pp">#endif </span><span class="co">// EMBED</span>
  <span class="cf">return</span> mg_solve ((scalar *){u}, (scalar *){r},
		   residual_viscosity, relax_viscosity, &amp;p, p.nrelax, p.res,
		   minlevel = <span class="dv">1</span>, <span class="co">// fixme: because of root level</span>
                                 <span class="co">// BGHOSTS = 2 bug on trees</span>
		   tolerance = TOLERANCE_MU);
}</code></pre></td></tr></table></div>
</div>
<div id="footer">
inspired by <a href="http://github.com/jgm/gitit/tree/master/">gitit</a>,
powered by <a href="/src/darcsit/README">darcsit</a>
</div>
          </div>
        </div>
        <div id="sidebar" class="yui-b first">
<div id="logo">
  <a href="/" alt="site logo" title="Go to top page"><img src="/img/logo.png" /></a>
</div>
          <div class="sitenav">
  <fieldset>
    <legend>Site</legend>
    <ul>
      <li><a href="/Front Page">Front page</a></li>
      <li><a href="/_index">All pages</a></li>
      <li><a href="/_activity">Recent activity</a></li>
            <li><a href="/Help">Help</a></li>
    </ul>
    <form action="/_search" method="get" id="searchform">
      <input type="text" name="patterns" id="patterns"/>
      <input type="submit" value="Search"/>
    </form>
  </fieldset>
  <fieldset>
    <legend>Documentation</legend>
    <ul>
      <li><a href="/Tutorial">Tutorial</a></li>
      <li><a href="/src/INSTALL">Installation</a></li>
      <li><a href="/Basilisk C">Basilisk C</a></li>
      <li><a href="/src/README">Solvers and functions</a></li>
      <li><a href="/src/examples/README">Examples</a></li>
      <li><a href="http://groups.google.com/d/forum/basilisk-fr">User forum</a></li>
      <li><a href="/sandbox/documentation">More documentation</a></li>
    </ul>
  </fieldset>
  <fieldset>
    <legend>Development</legend>
    <ul>
      <li><a href="/src/?history">Recent activity</a></li>
      <li><a href="/src/">Source code</a></li>
      <li><a href="https://hub.darcs.net/basilisk/basilisk/browse/src">Darcs Hub</a></li>
      <li><a href="/src/test/README">Test cases</a></li>
      <li><a href="/sandbox/bugs/README">Bug reports</a></li>
      <li><a href="/src/Contributing">How to contribute</a></li>
      <li><a href="/sandbox/">Play in the sandbox</a></li>
    </ul>
  </fieldset>
</div>
                    <div class="pageTools">
  <fieldset>
    <legend>This page</legend>
    <ul>
      <li><a href="/sandbox/ghigo/src/myviscosity.h?raw">Raw page source</a></li>
      <li><a href="/sandbox/ghigo/src/myviscosity.h?delete">Delete this page</a></li>
    </ul>
  </fieldset>
</div>
                            </div>
    </div>
<a id="" href="#"></a>
    <script type="text/javascript">
/* <![CDATA[ */
  $.get("/_user", {}, function(username, status) {
     $("#logged_in_user").text(username);
     if (username == "") {  // nobody logged in
        $("#logoutlink").hide();
        $("#loginlink").show();
     } else {
        $("#logoutlink").show();
        $("#loginlink").hide();
     };
   });
/* ]]> */
</script>
      </body>
</html>
