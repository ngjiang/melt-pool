import os
print("Download files from basilisk.fr");
# List of files to download
filename = [
'advection_A.h',
'alex_functions2.h',
'alex_functions.h',
'basic_geom.h',
'centered_alex.h',
'distance_point_ellipse3D.h',
'diverging_cool_warm.h',
'double_embed-tree.h',
'embed_extrapolate_3.h',
'emerged_NS.h',
'finite_diff.h',
'inertial.h',
'level_set.h',
'LS_advection.h',
'LS_curvature.h',
'LS_recons.h',
'LS_reinit.h',
'LS_speed.h',
'narrow_band.h',
'output_skeleton.h',
'phase_change_velocity.h',
'README',
'reinit_SF.h',
'run_A.h',
'simple_discretization.h',
'thinning.h',
'timestep-vdw.h',
'vdw3D.h',
'vdw.h',
'vof_rotation.h',
'weno2.h',
'weno5.h'
]
# folder from which to find the files...
webname="https://gitlab.com/justbobit/basilisk_sandbox_alimare/-/raw/master/alimare/"
#command...
for i in range(len(filename)):
  COMMAND = "wget -O "+filename[i]+" "+webname+filename[i]+"?raw"
  print(COMMAND)
  os.system(COMMAND)
