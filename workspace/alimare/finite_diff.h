/**
# undivided Finite-diff operators





*/
foreach_dimension()
static inline double uForDiff2_x(Point point, scalar a, int i, int j, int k){
 return (-3*a[i,j,k] + 4*a[i+1,j,k] - a[i+2,j,k])/2.;
}

foreach_dimension()
static inline double uBackDiff2_x(Point point, scalar a, int i, int j, int k){
 return (3*a[i,j,k] - 4*a[i-1,j,k] + a[i-2,j,k])/2.;
}


// no i!=0 indice because the maximum stencil is 5x5
foreach_dimension()
static inline double uCenGrad_x(Point point, scalar a, int j, int k){
  return (-a[-2,j,k] + 8*a[1,j,k] - 8*a[-1,j,k] + a[2,j,k])/12.;

}
foreach_dimension()
static inline double uCenLap_x(Point point, scalar a){
  return (-a[-2] - a[2] + 16*a[1] + 16*a[-1]-30*a[0])/12.;
}