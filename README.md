# melt pool
roadmap:
- Case definition (DONE)
- Implement Navier-Stokes without gravity /OK
- Implement Boussinesq's appromation /OK
- Convergence study with regular meshes
	- Measurement of pool width: /OK
		See lines commented in .c: "if(interfacial(point, cs) && ...){fprintf(...);}"
		Be careful: == is not redefined, so x should be M*(1/2)^n +/- (Delta)/2 
- (optional) AMR /OK

30/09/2021
- Output the final shape
	event final_shape(t=end){
		output_facets(cs)
	}
- Optimizing the time step (AL)
- Coarsing in the band (SP) /OK: amr-atan


11/10/2021 (with AL)
- Shown the first results
- Problem of sudden cold temperature: divergence
	- Added debug lines: dump(filename), bview2D filename, event... (see meltingPool.c)
	- Possible bug linked to LS_speed.h: NS time step not used
	- Adaptation to LS_diffusion.h still to be done
- Still to do: Nondimensionalization

08/12/2021
- INCREASE the viscosity to reproduce the double diffusion case
- decrease the time step
- change the numerical parameters
- have a new visualization method to zoom in (e.g. atomisation.c)
	option dump -DDUMP=1
	
- have a better resolution for the initial pool

Guassian: width = 4 * ecart-type

14/01/22
- Reference in movement
- change viscosity, power and laser speed.
- quantifications: form of the interface (Kuang-ling), width, depth

02/02/22
- Save the interface (qualitative v.s. real)
- Transitory: v = v_0 tanh(t/tau) (inspired from case of V*3)
- Time step 1/2, CFL (inspired from case of V/3)

For Monday:
Slides, input, initial, stational, interface
captions of film

08/02/22
- Measure of the extremal points (front, tail, depth in the centre of the laser and extremal depth)
- Velocity less violent: v=v_0*tanh(t/tau)
- Play with the time step (Kuan-ling)



