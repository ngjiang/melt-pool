\documentclass[fontsize=10pt]{article}
\usepackage[margin=0.70in]{geometry}
\usepackage{lipsum,mwe,abstract}
\usepackage[T1]{fontenc} 
\usepackage[french]{babel} 

\usepackage{fancyhdr} % Custom headers and footers
%\pagestyle{fancyplain} % Makes all pages in the document conform to the custom headers and footers
%\fancyhead{} 
%\fancyfoot[C]{\thepage} % Page numbering for right footer
\usepackage{lipsum}

\setlength\parindent{0pt} 
\setlength\parskip{.75em} 

\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
\usepackage{comment}
\usepackage{enumitem}
\usepackage{cuted}
\usepackage{sectsty} % Allows customizing section commands
\allsectionsfont{\normalfont \normalsize \scshape} % Section names in small caps and normal fonts

\renewenvironment{abstract} % Change how the abstract look to remove margins
 {\small
  \begin{center}
  \bfseries \abstractname\vspace{-.5em}\vspace{0pt}
  \end{center}
  \list{}{%
    \setlength{\leftmargin}{0mm}
    \setlength{\rightmargin}{\leftmargin}%
  }
  \item\relax}
 {\endlist}
 
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt} % Change how the title looks like
\begin{flushleft}
  \textbf{\@title} \\ [10pt]
  \@author \\ 
  \@date
\end{flushleft}\egroup
}
\makeatother

\usepackage{tikz}
\usetikzlibrary{%
    decorations.pathreplacing,%
    decorations.pathmorphing,%
    patterns
}
\usepackage{physics}

%% ------------------------------------------------------------------- 

\title{
\Large 
%Analyses of hydrodynamic instabilities in the melt pool in SLM
Analyses des instabilités hydrodynamiques dans la bain de fusion : adimensionnement
}
\date{\today}
\author{Nan Jiang, Christophe Josserand, Daniel Weisz-Patrault, Eric Charkaluk}

\begin{document}

 \maketitle

% --------------- ABSTRACT
%\begin{abstract}
%\end{abstract}

%\rule{\linewidth}{0.5pt}

% --------------- MAIN CONTENT

\section{Introduction}

Dans un premier temps, on ne considère que l'interaction entre le métal fondu et le métal solide supposé uniphasique dans 2D. Le problème à résoudre, montré dans la \textsc{Figure}~ \ref{fig:meltPool}, s'agit d'un morceau carré de métal solide avec un faisceau laser entrant en haut. Pour faciliter le calcul, il est supposé qu'il y ait une bain de fusion autour du faisceau laser initialement.

\begin{figure}[!htbp]
  \centering
  \input{fig_meltPool}
  \caption{Bain de fusion formée pendant la FA en SLM}
  \label{fig:meltPool}
\end{figure}

Dans le solide, on a l'équation du transport de la chaleur : 
\begin{equation}
  \label{eq:heat_S}
  \rho_S c_S \pdv{T_S}{t} = \div(\lambda_S \grad T_S).
\end{equation}

Dans le liquide, l'équation du transport de la chaleur s'écrit :
\begin{equation}
  \label{eq:heat_L}
  \rho_L c_L \left(\pdv{T_L}{t} + \vb{u}\vdot \grad T_L\right)
  = \div(\lambda_L \grad T_L).
\end{equation}

En plus, l'écoulement du corium dans la bain de fusion est décrite par les deux équations hydrodynamiques :
\begin{align}
  \label{eq:momentum}
  \rho_L \left(\pdv{\vb{u}}{t} + \vb{u} \vdot \grad \vb{u}\right)
  &= - \grad p_d + \div(2 \mu \vb{D}) + \rho_L \vb{f},
  \\
  \label{eq:continuity}
  \ \vb{u} &= 0,
\end{align}
où $\vb{D} = \frac{1}{2}\left(\grad \vb{u} + {}^\text{t} \grad \vb{u}\right)$ et $\vb{f}$ désignent respectivement le tenseur de vitesse de déformation et la flottabilité. 
Ici on introduit l'approximation de Boussinesq en supposant une faible variation de masse volumique liée avec la dilatation thermique. 
Selon l'approximation de Boussinesq, on a 
$\vb{f} = -\rho_L^{-1} \alpha (T_L - T_m) \vb{g}$,
où $\alpha$ est le coefficient de dilatation thermique. 
Enfin $p_d$ est une pression dite dynamique définie par $p_d = p - \rho_L g y$.

En terme du changement de phase, on suppose que la température soit la température de fusion tout au long du front : 
\begin{equation}
  \label{eq:T_eq}
  \forall \vb{x} \in \Gamma, \, T(\vb{x}, t) = T_m
  %- \varepsilon_\kappa \kappa - \varepsilon_v v_\text{pc}
  ,
\end{equation}
et la vitesse du changement de phase satifait la condition de Stefan :
\begin{equation}
  \label{eq:Stefan}
  \rho_S L_H \vb{v}_\text{pc} \vdot \vb{n} 
  = \left(\lambda_L \grad \eval{T_L}_\Gamma - \lambda_S \grad \eval{T_S}_\Gamma\right) \vdot \vb{n}.
\end{equation}

En terme des conditions aux limites, le faisceau laser en haut est simulé dans un premier temps par une condition de Neumann en fonction créneau : 
\begin{equation}
  \pdv{T}{y}(x,y,t) = 
  \left\{
  \begin{array}{ll}
    A,\;&\left|x-x_0(t)\right|<\frac{w}{2},\\
    0,\;&\text{sinon},
  \end{array}
  \right.
\end{equation}
où $x_0(t)$ désigne la position du centre du faisceau laser au cours du temps et $w$ est la largeur du faisceau laser.

Dans la réalité, le faisceau laser impose un flux thermique sur une surface assimilé par une disque de diamètre $w$ avec une puissance total $\mathcal{P}$, soit
\begin{equation*}
  \lambda_L \grad T \vdot \vb{n} \frac{\pi w^2}{4} = \mathcal{P},
\end{equation*}
ce qui nous donne :
\begin{equation}
  \pdv{T}{y} = \frac{4 \mathcal{P}}{\pi \lambda_L w^2}.
\end{equation}

On pourrait aussi assimiler la distribution de la puissance par une fonction gaussiennne : 
\begin{equation}
  \pdv{T}{y} = A e^{-\frac{\left[x - x_0(t)\right]^2}{2 \sigma^2}}
\end{equation}
En connaissant l'intégrale sur la surface d'une fonction gaussienne donne : $\int_{\mathbb{R}^2} e^{-\frac{\left[x - x_0(t)\right]^2}{2 \sigma^2}} \dd{x} = 2 \pi \sigma^2 $ et $w = 4 \sigma$, on suppose que
\begin{equation}
  \lambda_L 2\pi\left(\frac{w}{4}\right)^2 A = \mathcal{P},
\end{equation}
ce qui nous donne
\begin{equation}
  A = \frac{8 \mathcal{P}}{\pi \lambda_L w^2}.
\end{equation}


Sur les trois autres côtés, le solide est refroidi par les conditions de Dirichlet :
\begin{equation}
  T = T_\infty.
\end{equation}

\section{Adimensionnement des équations}

Dans un premier temps, on définit $L_0$ la largeur du carré comme l'échelle du longueur. En raison de la simplicité de l'équation de la chaleur dans le solide (\ref{eq:heat_S}), l'échelle du temps est prise à $\tau = L_0^2 / D_S$, où $D_S = \frac{\lambda_S}{\rho c_S}$ est la diffusivité thermique dans le solide. 
Ainsi, les liens entre les opérateurs dimensionnels et adimensionnels sont donnés par $\grad = \frac{1}{L_0} \grad^\star$ et $\pdv{t} = \frac{D_S}{L_0^2} \pdv{t^\star}$. De même, $\vb{u} = \frac{D_S}{L_0} \vb{u}^\star$.
Finalement, la température adimensionnelle est définie comme suivante :
\begin{equation}
  \theta_\bullet = \frac{T_\bullet - T_m}{T_m - T_\infty},
\end{equation} 
qui conduit à $T_\bullet = (T_m - T_\infty) \theta_\bullet + T_m$.

\subsection{Equations du transport de la chaleur}

L'équation (\ref{eq:heat_L}) conduit à 
\begin{equation*}
  \frac{(T_m - T_\infty) \rho c_S D_S}{L_0^2} \left(\pdv{\theta_L}{t^\star} + \vb{u}^\star \vdot \grad^\star \theta_L\right)
  = \grad^\star\vdot \left(\frac{\lambda_L}{L_0^2} (T_m - T_\infty) \grad^\star \theta_L\right)
\end{equation*}
soit
\begin{equation}
  \pdv{\theta_L}{t^\star} + \vb{u}^\star \vdot \grad^\star \theta_L
  = \grad^\star\vdot \left(\frac{D_L}{D_S} \grad^\star \theta_L\right).
\end{equation}

L'adimensionnement de l'équation (\ref{eq:heat_S}) est donc immédiat :
\begin{equation}
  \pdv{\theta_S}{t^\star} = {\nabla^\star}^2 \theta_L.
\end{equation}

\subsection{Equations de Navier-Stokes}

Il est évident que la forme adimensionnelle de l'équation (\ref{eq:continuity}) s'écrit 
\begin{equation}
  \grad^\star \vdot \vb{u}^\star = 0.
\end{equation}

Avec l'équation (\ref{eq:momentum}), on a :
\begin{equation*}
  \rho_L \frac{D_S^2}{L_0^3} \left(\pdv{\vb{u}^\star}{t^\star} + \vb{u}^\star \vdot \grad^\star \vb{u}^\star\right)
  = -\frac{1}{L_0} \grad^\star p_d + \grad^\star \vdot \left(\frac{2\mu D_S}{L_0^3} \vb{D}^\star\right) - \alpha (T_L - T_m) \vb{g},
\end{equation*}
où $\vb{D}^\star = \frac{D_S}{L_0^2} \vb{D}$, donc 
\begin{equation*}
  \pdv{\vb{u}^\star}{t^\star} + \vb{u}^\star \vdot \grad^\star \vb{u}^\star
  = - \grad^\star \left(\frac{L_0^2}{\rho_L D_S^2} p_d\right) + \grad^\star \vdot \left(\frac{2 \mu}{\rho_L D_S} \vb{D}\right)
  + \frac{\alpha (T_L - T_m) g L_0^3}{D_S^2} \vb{e}_y,
\end{equation*} 
soit
\begin{equation}
  \pdv{\vb{u}^\star}{t^\star} + \vb{u}^\star \vdot \grad^\star \vb{u}^\star
  = - \grad^\star p^\star + \grad^\star \vdot \left(2 \text{Pr}\, \vb{D}\right)
  + \text{Ra}\,\text{Pr}\, \vb{e}_y,
\end{equation}
où on définit 
$p^\star = \frac{L_0^2}{\rho_L D_S^2} p_d$,
le nombre de Prandtl $\text{Pr} = \frac{\mu}{\rho_L D_S}$
et le nombre de Rayleigh $\text{Ra} = \frac{\alpha (T_L - T_m) \rho_L g L_0^3}{\mu D_S}$.

\subsection{Equations du changement de phase}

L'équation (\ref{eq:T_eq}) s'écrit en forme adimensionnelle comme :
\begin{equation}
  \forall \vb{x} \in \Gamma,\,
  \theta(\vb{x}, t) = 0.
\end{equation}

Finalement, en remplaçant par les quantités adimensionnelles, l'équation (\ref{eq:Stefan}) s'écrit : 
\begin{equation*}
  \rho_S L_H \frac{D_S}{L_0} \vb{v}_\text{pc}^\star \vdot \vb{n}
  = \frac{\lambda_L}{L_0} (T_m - T_\infty) \grad^\star \theta_L
  - \frac{\lambda_S}{L_0} (T_m - T_\infty) \grad^\star \theta_S,  
\end{equation*}
qui entraîne :
\begin{equation*}
  \vb{v}_\text{pc}^\star \vdot \vb{n} 
  = \frac{\lambda_S (T_m - T_\infty)}{\rho_S L_H D_S} 
  \left(\frac{\lambda_L}{\lambda_S} \grad^\star \theta_L - \grad^\star \theta_S\right)
\end{equation*}
soit
\begin{equation}
  \vb{v}_\text{pc}^\star \vdot \vb{n} 
  = \text{St}
  \left(\frac{\lambda_L}{\lambda_S} \grad^\star \theta_L - \grad^\star \theta_S\right)
\end{equation}
où on définit le nombre de Stefan : $\text{St} = \frac{\rho_S c_S D_S (T_m - T_\infty)}{\rho_S L_H D_S} = \frac{c_S (T_m - T_S)}{L_H}$.

\subsection{Conditions aux limites}

Sur le côté en haut, le faisceau laser est représenté soit par une fonction créneau : 
\begin{equation}
  \pdv{\theta_{L,S}}{y^\star} = 
  \left\{
  \begin{array}{ll}
    \frac{4 \mathcal{P}}{\lambda_L \pi w^2} \frac{L_0}{T_m - T_\infty},\;
      & \left|x^\star-x_0^\star(t^\star)\right| < w/L_0,\\
    0,\;&\text{sinon}.
  \end{array}
  \right.
\end{equation}
soit par une fonction gaussienne : 
\begin{equation}
  \pdv{\theta_{L,S}}{y^\star} = \frac{8 \mathcal{P}}{\lambda_L \pi w^2} \frac{L_0}{T_m - T_\infty}
  \exp{-\frac{\left[x^\star-x^\star_0(t)\right]^2}{2\left(\frac{w^\star}{4}\right)^2}}
  .
\end{equation}

Sur les autres côtés, les conditions de Dirichlet se traduit en
\begin{equation}
  \theta_S = -1.
\end{equation}

\subsection{Conclusion}
En résumé, les paramètres apparaissant dans les équations adimensionnelles sont :
les rapports des diffusivités thermiques $D_L/D_S$ et des conductivités $\lambda_L/\lambda_S$,
le nombre de Prandtl $\text{Pr} = \frac{\mu}{\rho_L D_S}$,
le nombre de Rayleigh $\text{Ra} = \frac{\alpha (T_L - T_m) \rho_L g L_0^3}{\mu D_S}$ 
et le nombre de Stefan $\text{St} = \frac{c_S (T_m - T_S)}{L_H}$.
En plus de ces paramètres, il faut aussi connaître 
$\pdv{\theta}{y^\star}$ le gradient adimensionnel de la température sur le côté haut
pour définir les conditions aux limites.

Ceci fait intervenir les propriétés suivantes : 
$\rho_S$ et $\rho_L$ les masses volumiques dans les deux phases,
$c_S$ et $c_L$ les capacités thermiques (isobares) massiques,
$\lambda_S$ et $\lambda_L$ les conductivités thermiques,
$\mu$ la viscosité (dynamique) dans la phase liquide,
$T_m$ la température de fusion/solidification et
$L_H$ la chaleur latente.
Pour déterminer les conditions aux limites, il faut connaître 
$w$ la largeur et
$\mathcal{P}$ la puissance 
du faisceau laser.
En plus, il faut aussi définir   
$L_0$ la largeur du carré de calcul,
$g$ l'accélération de la pesanteur.


\section{Estimation des ordres de grandeur : cas d'Inconel 718}
Puisque $\rho_L \simeq \rho_S$ pour un métal, on notera $\rho = \rho_L = \rho_S$ dans la suite.


\begin{thebibliography}{}
%\bibitem{Ref1}{First reference}
\end{thebibliography}


\end{document}
