import os
import numpy as np
import matplotlib
import pickle
import matplotlib.pyplot as plt

#matplotlib.use("pgf")
matplotlib.rcParams.update({
#    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
#    "font.serif": ["Times New Roman"],
#    'pgf.rcfonts': False,
})

#os.system("cat U | tr -d '()' > U.noBrackets")
os.system("cp meltPool/log5 log5.tmp")
os.system("sed -i '$ d' log5.tmp")
mydata=np.loadtxt("log5.tmp",dtype=np.float)
os.system("rm -f log5.tmp")
#os.system("rm U.noBrackets")



fig1=plt.figure(1)
ax=plt.subplot(111)
plt.plot(mydata[:,0],mydata[:,1],label='Maximal depth')
plt.plot(mydata[:,0],mydata[:,2],label='Central depth')
plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax.legend(loc='upper right')
plt.xlabel('Time $t D_S / L^2$');
plt.ylabel('Melt pool depth $d/L$');
fig1.savefig("depth.pdf")

fig2=plt.figure(2)
ax=plt.subplot(111)
plt.plot(mydata[:,0],mydata[:,3],label='Left edge')
plt.plot(mydata[:,0],mydata[:,4],label='Right edge')
plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax.legend(loc='lower right')
plt.xlabel('Time $t D_S / L^2$');
plt.ylabel('Melt pool edge $x/L$');
fig2.savefig("edge.pdf")

fig3=plt.figure(3)
ax=plt.subplot(111)
plt.plot(mydata[:,0],mydata[:,5],label='Width')
plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax.legend(loc='lower right')
plt.xlabel('Time $t D_S / L^2$');
plt.ylabel('Melt pool width $w/L$');
fig3.savefig("width.pdf")



#import tikzplotlib
#tikzplotlib.save('myfig1.tex')
