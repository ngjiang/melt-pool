import os
print("Download files from basilisk.fr");
# List of files to download
filename = [
'mycentered.h',
'mycentered2.h',
'mydiffusion.h',
'mydoubleprojection.h',
'myembed-force.h',
'myembed-moving-color.h',
'myembed-moving.h',
'myembed-particle-color.h',
'myembed-particle.h',
'myembed-tree-moving.h',
'myembed.h',
'myperfs.h',
'mypoisson.h',
'myquadratic.h',
'myspheroid.h',
'mytimestep.h',
'mytracer.h',
'myviscosity-embed.h',
'myviscosity-viscoplastic.h',
'myviscosity.h '
]
# folder from which to find the files...
webname="http://www.basilisk.fr/sandbox/ghigo/src/"
#command...
for i in range(len(filename)):
  COMMAND = "wget -O "+filename[i]+" "+webname+filename[i]+"?raw"
  print(COMMAND)
  os.system(COMMAND)
