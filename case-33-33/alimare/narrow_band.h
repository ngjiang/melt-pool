void Narrow_band_cache(Cache * c, scalar dist){
// c has been initialized
  scalar flag[],flag0[];
  foreach(){
    flag[]     = 0;
    flag0[]     = 0;
  }
  boundary({flag,flag0});

  foreach(){
    double tag = 1.;
    foreach_dimension(){
      tag = min (tag, dist[-1,0,0]*dist[]);
      tag = min (tag, dist[ 1,0,0]*dist[]);
    }
    if(tag < 0.){
      foreach_dimension()
        flag0[1]  = 1;
        flag0[-1] = 1;
        flag[1]   = 1;
        flag[-1]  = 1;
    }
  }
  boundary({flag0,flag});

  foreach()
    if(flag0[]){
      foreach_dimension()
        flag[1] = 1;
        flag[-1] = 1;
    }
  boundary({flag});
  foreach()
    if(flag[])
      cache_append (c, point, 0);

  cache_shrink (c);
}