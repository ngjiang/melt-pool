#!/bin/bash

while [ -n "$1" ]; do # while loop starts
	case "$1" in
	-p) 
	    echo "Preparing the test case file meltPool.c ..."
	    python case.py
	    #shift
	    ;;
	-r) 
	    echo "Running the test case file meltPool.c ..."
	    make meltPool.tst
	    shift
	    ;;
	-s)
		savename=meltPool_$(date +%Y%m%d)_$(date +%H%M%S)
		echo "Saving current folder to ../saves/$savename.zip ..."
        zip -q -r ../saves/$savename.zip *
		#shift
		;;
	-c) 
	    echo "Cleaning the workspace ..."
	    make clean
	    rm meltPool.c depth.pdf edge.pdf width.pdf
	    shift
	    ;;
	*) echo "Option $1 not recognized" ;;
	esac
	shift
done
