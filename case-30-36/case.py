import re
import template.dimension as temp
import dimension as test



readfile = "template/meltPool.c"

with open(readfile, 'r') as rf:
    text = rf.read()
    text = re.sub('%0.4f' % temp.DR, '%0.4f' % test.DR, text)
    text = re.sub('%0.5f' % temp.Pr, '%0.5f' % test.Pr, text)
    text = re.sub('%0.1f' % temp.Ra, '%0.1f' % test.Ra, text)
    text = re.sub('%0.3f' % (temp.V_laser/temp.V), '%0.3f' % (test.V_laser/test.V), text)
    text = re.sub('%0.4f' % temp.St, '%0.4f' % test.St, text)
    text = re.sub('%0.7f' % temp.wStar, '%0.7f' % test.wStar, text)
    text = re.sub('%0.3f' % temp.dTdy, '%0.3f' % test.dTdy, text)
    text = re.sub('%0.5f' % temp.T_end, '%0.5f' % test.T_end, text)
    #f.seek(0)
    #f.write(text)
    #f.truncate()
    writefile = 'meltPool.c'
    wf = open(writefile, 'w+')
    wf.write(text)



