import os
import numpy as np
import matplotlib
import pickle
import matplotlib.pyplot as plt

#matplotlib.use("pgf")
matplotlib.rcParams.update({
#    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
#    "font.serif": ["Times New Roman"],
#    'pgf.rcfonts': False,
})

title_name='$v$=27 mm/s'
P=[27,30,33]
data=[]
for i in range(len(P)):
	#os.system("cat U | tr -d '()' > U.noBrackets")
	cmd_name="cp ../../case-%d-27/meltPool/log5 log5.tmp" % P[i]
	#os.system("cp meltPool/log5 log5.tmp")
	os.system(cmd_name)
	os.system("sed -i '$ d' log5.tmp")
	data+=[np.loadtxt("log5.tmp",dtype=np.float)]
	os.system("rm -f log5.tmp")
	#os.system("rm U.noBrackets")

################################################################################

figs=[]
fig_titles=['Maximal Depth', 'Central Depth', 'Left Edge', 'Right Edge', 'Width']
y_labels=['Depth $d/L$', 'Depth $d/L$', 'Edge $x/L$', 'Edge $x/L$', 'Width $w/L$']
fig_names=['max_depth', 'central_depth', 'left_edge', 'right_edge', 'width']

for j in range(5):
	figs+=[plt.figure(j+1)]
	ax=plt.subplot(111)
	for i in range(len(P)):
		#plt.plot(mydata[:,0],mydata[:,1],label='Maximal depth')
		#plt.plot(mydata[:,0],mydata[:,2],label='Central depth')
		plt.plot(data[i][:,0], data[i][:,j+1],label="$P$=%d W" % P[i])
	plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
	ax.legend(loc='upper left')
	plt.title(fig_titles[j] + ' of the Melt Pool, ' + title_name)
	plt.xlabel('Time $t D_S / L^2$');
	plt.ylabel(y_labels[j]);
	figs[j].savefig(fig_names[j]+'.pdf')

