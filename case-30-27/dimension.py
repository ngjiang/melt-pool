# in SI

# mechanics
rho = 8.e3
g = 9.81

# phase change
T_eq = 1600.
T_inf = 300.
LH = 2.35e9 / rho

# thermodynamics
lambdaS = 18.3
lambdaL = 21.
CS = 0.435e3
CL = 0.435e3
DS = lambdaS / rho / CS
DL = lambdaL / rho / CL 
alpha = 10.e-6

# fluid mechanics
mu = 3.e-3
nu = mu/rho

# laser beam
W_laser = 0.338e-3
P_laser = 30.
V_laser = 27.e-3

# geometry
L = 1e-2
pi = 3.14159265



#print("DS =", DS, "\nDL =", DL, "\n")

# calculations
DeltaT = T_eq - T_inf

DR = DL/DS
Pr = mu/rho/DS
Ra = g * alpha * DeltaT * L*L*L / nu / DL
tau = L*L / DS
V = DS / L
St = CS * DeltaT / LH
wStar = W_laser / L
# Square distribution, Gaussian distribution is double
dTdy = L / DeltaT * P_laser / (W_laser * W_laser * pi / 4.) / lambdaL 
T_end = 0.6 / V_laser * V 


if __name__=="__main__":
    print("Diffusivity Ratio =", DR)
    print("Pr =", Pr)
    print("Ra =", Ra)
    print("V_laser =", V_laser/V)
    print("St =", St)
    print("W_laser =", wStar)
    print("dTdy =", dTdy)
    print("T_end =", T_end)

