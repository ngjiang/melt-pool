import os
import numpy as np
import matplotlib
import pickle
import matplotlib.pyplot as plt

#matplotlib.use("pgf")
matplotlib.rcParams.update({
#    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
#    "font.serif": ["Times New Roman"],
#    'pgf.rcfonts': False,
})

P = np.array([30., 27., 33., 30., 30., 24., 27., 27., 30., 33., 33., 30.])
V = np.array([30., 30., 30., 27., 33., 30., 27., 33., 24., 27., 33., 36.])
colors = np.array([1,1,0,1,1,0,1,0,1,1,1,1])
colorLegend = ['Non functional', 'Basically functional', 'Under computation']

fig1=plt.figure(1)
ax=plt.subplot(111)
for color in range(2):
    plt.scatter(V[colors==color], P[colors==color], label=colorLegend[color])
plt.xlabel('Speed (mm/s)')
plt.ylabel('Power of laser (W)')
ax.legend()
#ax.grid(True)
fig1.savefig("pvDiagram.pdf")

